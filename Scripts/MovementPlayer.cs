﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour {

    private Rigidbody2D _rb2d;

	// Use this for initialization
	void Start () {
        _rb2d = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //float moveHorizontal = Input.GetAxis("Horizontal");

            ////Store the current vertical input in the float moveVertical.
            //float moveVertical = Input.GetAxis("Vertical");

            ////Use the two store floats to create a new Vector2 variable movement.
            //Vector2 movement = new Vector2(moveHorizontal, moveVertical);
            //Debug.Log(transform.position);
            //_rb2d.AddForce(movement * 10);
            //transform.position += new Vector3(0.1f, 0, 0);
        }
            
        if (Input.GetKey(KeyCode.LeftArrow))
            transform.position -= new Vector3(0.1f, 0, 0);
        if (Input.GetKey(KeyCode.UpArrow))
            _rb2d.AddForce(new Vector2(0, -1));
    }
}
